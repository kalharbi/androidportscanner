# Port Scanner
### Questions:

**1. Test to see if port 80 and 443 are open.**

   This can be done by creating a TCP socket with a given host name and port number and then using the API method _Socket.isConnected()_ to test if it's open or not. See the _socketConnect_ method in _ScannerThread_ class

**2. Modify the profile as if you were going to test to see if it was running ssh?  Does it connect?  If not, why?**

   SSH runs on port number 22, so we need to add  port number 22 to the _ScanProfile.QuickScan_ class and check the result in the _socketConnect_ method of _ScannerThread_.

   It may connect if the host machine is listening on port 22. For example, the socket was able to connect to a remote server when I started ssh server process on a remote server and entered its IP address and port 22.

**3.  What profile might you use to see if a system is running Windows?  MAC?  Android?**

I do not think that Java can do TCP fingerprinting, and I do not believe it's even reliable to try that since most modern operating systems use TCP/IP fingerprint obfuscators to confuse TCP/IP fingerprinting tools. One possible way to try that is to use binary tools such as netmap and execute it as a subprocess in your Java code and then read its output stream, or use a wrapper for that such as [this wrapper](http://sourceforge.net/projects/nmap4j).

###Extra Credit:

**1. How could you extend the code to start a two-way conversation with the port?**

Once the socket is connected, we need to get its output and input streams. Then, we can use the output stream to send data through the socket to the host machine and port number. See the _socketConnect_ method of the _ScannerThread_ example.

    Example:

        if (socket.isConnected()) {
        // once is connected, send a message to the socket and get the response			
        // Get the socket output and input streams
	    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	    BufferedReader in = new BufferedReader(new InputStreamReader(
	    socket.getInputStream()));
	    // Send data through the socket to the host machine
	    out.println("Hello");
	    Log.i(TAG, "echo: " + in.readLine());
        }

