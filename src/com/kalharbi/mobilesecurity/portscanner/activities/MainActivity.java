package com.kalharbi.mobilesecurity.portscanner.activities;


import java.util.List;
import com.kalharbi.mobilesecurity.portscanner.R;
import com.kalharbi.mobilesecurity.portscanner.ScannerThread;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final String TAG = MainActivity.class.getSimpleName();

	private Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		init();
	}

	private void init() {
		// set the default IP address
		((EditText) findViewById(R.id.editText_ip))
				.setText(getLocalIpAddress());
		registerHandler();
	}

	private void registerHandler() {

		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case 2001: // TCP Scan
					List<Integer> livePorts = (List<Integer>) msg.obj;
					String stringPorts = "";

					if (livePorts != null && livePorts.size() > 0) {
						for (Integer livePort : livePorts) {
							stringPorts = stringPorts + ":"
									+ livePort.toString();
						}
					}
					// show the result
					if (stringPorts == "") {
						stringPorts = "None";
					}
					((TextView) findViewById(R.id.resultsTextView))
							.setText(stringPorts);
					Log.i(getClass().getSimpleName(), "Ports found: "
							+ stringPorts);
					break;
				default:
					break;
				}

			}
		};
	}

	public void doClick(View view) {
		final EditText etIP = (EditText) findViewById(R.id.editText_ip);
		new Thread(new ScannerThread(etIP.getText().toString(), handler))
				.start();
		Toast.makeText(getApplicationContext(), "scanner started",
				Toast.LENGTH_LONG).show();
	}

	private String getLocalIpAddress() {
		WifiManager wifiManager = (WifiManager) getSystemService(WIFI_SERVICE);
		WifiInfo wifiInfo = wifiManager.getConnectionInfo();
		return Formatter.formatIpAddress(wifiInfo.getIpAddress());
	}

}
