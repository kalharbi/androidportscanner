/**
Khalid
 */
package com.kalharbi.mobilesecurity.portscanner;

public class ScanProfile {
	public class QuickScan {
		private Integer[] tcp = { 80, 443, 22 };

		public Integer[] getTcpProfile() {
			return tcp;
		}
	}
}
