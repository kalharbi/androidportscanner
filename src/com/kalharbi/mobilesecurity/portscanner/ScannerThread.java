/**
Khalid
 */
package com.kalharbi.mobilesecurity.portscanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class ScannerThread implements Runnable {
	private static final String TAG = ScannerThread.class.getSimpleName();

	private String target;
	private List<Integer> livePorts;
	Handler handler;
	Context c;
	private int timeout = 1000;

	private Socket socket;

	public ScannerThread(String _target, Handler _handler) {
		target = _target;
		handler = _handler;
		livePorts = new ArrayList<Integer>();
	}

	public int socketConnect(String _target, int _port, int _timeout) {
		try {
			socket = new Socket();
			SocketAddress socketAddress = new InetSocketAddress(_target, _port);
			socket.connect(socketAddress, _timeout);
			if (socket.isConnected()) {
				// once is connected, we can send a message to the socket and get the response
				
				// Get the socket output and input streams
				PrintWriter out = new PrintWriter(socket.getOutputStream(),
						true);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				// Send data through the socket to the host machine
				out.println("Hello");
				Log.i(TAG, "echo: " + in.readLine());
				return 1;
			} else {
				return -1;
			}

		} catch (UnknownHostException e) {
			return -2;
		} catch (IOException e) {
			return -3;
		} catch (Exception e) {
			return -4;
		}
	}

	public void sendTcpResults() {
		Message status = new Message();
		status.what = 2001;
		status.obj = livePorts;
		handler.sendMessage(status);

	}

	@Override
	public void run() {
		ScanProfile profile = new ScanProfile();
		Integer[] tcpPorts;

		ScanProfile.QuickScan quickScan = profile.new QuickScan();
		tcpPorts = quickScan.getTcpProfile();

		for (int i = 0; i < tcpPorts.length; i++) {
			if (socketConnect(target, tcpPorts[i], timeout) == 1) {
				livePorts.add(tcpPorts[i]);
			}
		}

		sendTcpResults();
	}

}
